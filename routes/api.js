const express = require('express')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const Post = require('../controllers/todo.js')
const post = new Post

const logger = (req,res,next) => {
    console.log('LOG: ${req.method} ${req.url')
    next()
}

const api = express.Router()

api.use(logger)
api.use(jsonParser)

api.post('/todo/', post.Post)
api.put('/done/:index', jsonParser, post.insertPost)
api.get('/todo?isDone', jsonParser, post.updatePost)
api.delete('/todo/:index', post.deletePost)

module.exports = api