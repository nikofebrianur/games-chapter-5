const messages = {
    "200": "Sukses",
    "201": "To-do tersimpan"
}

function successResponse(
        res,
        code,
        data,
        meta = {}
) {
    res.status(code).json({
        data: data,
        meta: {
            code: code,
            message: messages[code.toString()],
            ...meta 
        }
    })
}

module.exports = {
    successResponse
}